Welcome to `sim_runner` documentation!
======================================

This tool aims to streamline and partially automatize running the Monte-Carlo simulations for
Imaging Athmosperic Cherenkov Telescopes. It relies on the `sim_telarray <https://www.mpi-hd.mpg.de/hfm/~bernlohr/sim_telarray/>`_.
For the convenience of usage the main configuration tree is supposed to be stored in git VCS and a local config allows to override 
user-defined parameters to address user needs.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   sim_runner
   CONTRIBUTING
   DEVELOPERS

Quick start guide
=================
Prerequisites:
    Please see `development setup <DEVELOPERS.html#a-name-setup-development-setup>`_

Clone the code from the repository::

    git clone https://gitlab.cern.ch/cta-unige/sim_runner.git && cd sim_runner

Install the code::

    flit install --user

Prepare the configuration file (you can use the files under examples/ as a reference) and launch::

    sim_runner -c /path/to/configuration.toml

Enjoy!

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
