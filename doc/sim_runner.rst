Detailed documentation
**********************

`sim_runner` configuration
==========================

Configuration is provided through TOML file.
It is separated in following sections:

- `environment`: this section contains fields related to the running environment:
  which version of the simulation software to run, where to take the configuration
  for it, and where the input and output files should be located.

- `process`: describe the simulation process you want to run. 
  Parametric scan may be of particular interest. Specify the name of the parameter of interest, min and max values and the step.

- `corsika`: Corsika support added. As default functionality it reads an input configuration, modifies the seed, run number and provides an output file name.
  It can also scan a parameter. An additional functionality is the parameter_card. There, the user should provide a list with the names of parameters they
  wish to modify, plus a list of lists, where the values of these parameters are stated. Sim_runner goes through the list, assigns the values to the
  parameters, and launches the simulation.

- `sim_telarray`: `sim_telarray` options you would like to override with respect to its main
  configuration provided in the `environment` section. The layout of this section follows the
  official `sim_telarray`
  `documentation <https://www.mpi-hd.mpg.de/hfm/~bernlohr/sim_telarray/Documentation/sim_hessarray.pdf>`_.

See below the example of configuration file with inline comments. Other configuration examples can be found under `examples` directory.

.. code-block:: python

    # Configuration file for IACT simulations configured for CI in docker container
    [environment]
    software_intallation_path = '/opt/sim_telarray/bin'
        [environment.submission]
        run_directory = "/tmp/sim_runner"
        tool = "local" # Either local, sge or slurm are supported
        [environment.corsika]
        version = "6.9"
        temporary_local_configuration_path = '/tmp/corsika-config'
    
        [environment.sim_telarray]
        version = "2018-11-07"
        temporary_local_configuration_path = '/tmp/sim_telarray-config'
        configuration_repository = 'https://gitlab.cern.ch/cta-unige/sim-config'
        tag = "LST-SiPM-NSB-0.1"
        # Main configuration file in the tagged configuration, which links the rest configuration files together
        entry_point = "config.cfg"
        input_path = "./examples/data/"
        output_path = "./test/output/"
        # Expert-only options
        # Change this option to use non-versionned config
        # Use at your own risk. Not recommended.
        # The config has to be placed under temporary_local_configuration_path
        use_non_tagged_configuration = false
    
    [process]
    dry_run = false # Read all configurations, generate job log header run actual simulation
    stage = 'sim_telarray' # could be either corsika or sim_telarray. For the moment only latter is supported
    production_type = "gamma"
    extract_calibration = false
        [process.parameter_scan]
        particle_type = 'gamma'
        parameter_block = 'trigger'
        parameter = 'asum_threshold'
        initial = 200
        final = 400
        step = 100

    stage = 'corsika' # could be either corsika or sim_telarray
    production_type = "parameter_card"
	[process.parameter_card]
	particle_type = 'proton'
	parameter_list = ['viewcone', 'theta', 'CSCAT', 'Emin', 'Emax']
	parameter_values = [['8.0', '0.0', '1000.0', '0.1E3', '100.0E3'], ['10.0', '20.0', '1500.0', '0.2E3', '200.0E3']]
    
    [corsika]
    # number of corsika jobs to be launched with the same input parameters
    number_of_runs = 2
    
    [sim_telarray]
        [sim_telarray.preprocessor]
        NUM_TELESCOPES = 1
    
        [sim_telarray.global_parameters]
        min_photoelectrons = -1 # default
        min_photons = -1 # default
        only_triggered_telescopes = 0
        only_triggered_arrays = 0
        maximum_events = 100
        ignore_telescopes= 'all:-1'
        maximum_telescopes = 7
        random_state = 'auto'
        show = 'all' # Show all parameters at the beginning of each job
    
        [sim_telarray.telescope]
        # Common orientations: 'north': 'phi' = 5.3195}, 'south': 'phi' = 174.6805
        telescope_phi = 174.6805
        telescope_zenith_angle = 20 # commonly named θ
    
        [sim_telarray.night_sky_background]
        nsb_scaling_factor = 2
    
        [sim_telarray.calibration]
        pedestal_events = 10 # check default value! 100 is for pedestal runs
    
        [sim_telarray.trigger]
        asum_threshold = 1000
    
    [meta]
    telescope_array_name = "lst"

sim_runner usage
================
.. argparse::
    :module: sim_runner.scripts.sim_runner 
    :func: get_parser
    :prog: sim_runner


API documentation
=================

:mod:`sim_runner.core.runner`
-----------------------------
.. automodule:: sim_runner.core.runner
    :members:
