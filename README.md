# sim_runner

Welcome to `sim_runner` project. This tool allows smooth and controlled running of the IACT simulation chain.

You can find quick start guide together with detailed API documentation on the [sim_runner  website][documentation_website].

If you would like to contribute to the project please start with reading the [Contributor's Guide][contributors].

Enjoy!

[contributors]:CONTRIBUTING.md
[setup]:DEVELOPERS.md#setup
[documentation_website]:https://ctaunigesw-sim-runner.web.cern.ch/ctaunigesw-sim-runner/
