#!/usr/bin/env python3
"""
Run simulation chain
"""
import argparse
from sim_runner.core import runner


def get_parser():
    """
    Create command line options parser

    :return: Parser object
    :rtype: class `argparse.ArgumentParser`
    """
    parser = argparse.ArgumentParser()
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument('-c', '--config', help='Simulation configuration file name',
                                required=True)
    parser.add_argument('-v', '--verbosity', help='Logging verbosity level', type=int,
                        choices=[0, 1, 2, 3, 4], default=1)
    return parser


def main():
    """
    Parse command line input arguments and launch the sim_runner.
    """
    parser = get_parser()
    args = parser.parse_args()
    runner.run(args.config, args.verbosity)


if __name__ == "__main__":
    main()
